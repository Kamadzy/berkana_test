<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $admin=new \App\User();
        $admin->name='admin';
        $admin->email='lapchenkovbyu@gmail.com';
        $admin->password=Hash::make('123123');
        $admin->save();
    }
}
