@include('shop.layouts.header')
{{--@include('shop.layouts.slider')--}}
<div class="items">
	 <div class="container">
		 @if(!$category_show->isEmpty())
		 <div>
			 <br>
			 <h2 class="text-center">Categories</h2>
			 <br>
		 </div>
		 <div class="items-sec">
			 @foreach($category_show as $cat)
			 <div class="col-md-3 feature-grid">
				 <a href="#"><img src="/{{$category->image}}" alt=""/>
					 <div class="arrival-info">
						 <h4 class="text-center">{{$cat->name}}</h4>
					 </div>
					 <div class="viw">
						<a href="#">
							<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>View</a>
					 </div>
				  </a>
			 </div>
			 @endforeach
			 <div class="clearfix"></div>
		 </div>
		 @endif
	 </div>
</div>

<!---->
<div class="offers">
	 <div class="container">
	 {{--<h3>End of Season Sale</h3>--}}
	 <div class="offer-grids">
		 {{--<div class="col-md-6 grid-left">--}}
			 {{--<a href="#"><div class="offer-grid1">--}}
				 {{--<div class="ofr-pic">--}}
					 {{--<img src="images/ofr2.jpeg" class="img-responsive" alt=""/>--}}
				 {{--</div>--}}
				 {{--<div class="ofr-pic-info">--}}
					 {{--<h4>Emergency Lights <br>& Led Bulds</h4>--}}
					 {{--<span>UP TO 60% OFF</span>--}}
					 {{--<p>Shop Now</p>--}}
				 {{--</div>--}}
				 {{--<div class="clearfix"></div>--}}
			 {{--</div></a>--}}
		 {{--</div>--}}
		 {{--<div class="col-md-6 grid-right">--}}
			 {{--<a href="#"><div class="offer-grid2">--}}
				 {{--<div class="ofr-pic-info2">--}}
					 {{--<h4>Flat Discount</h4>--}}
					 {{--<span>UP TO 30% OFF</span>--}}
					 {{--<h5>Outdoor Gate Lights</h5>--}}
					 {{--<p>Shop Now</p>--}}
				 {{--</div>--}}
				 {{--<div class="ofr-pic2">--}}
					 {{--<img src="images/ofr3.jpg" class="img-responsive" alt=""/>--}}
				 {{--</div>--}}
				 {{--<div class="clearfix"></div>--}}
			 {{--</div></a>--}}
		 {{--</div>--}}
		 <div class="clearfix"></div>
	 </div>
	 </div>
</div>
<!---->
<div class="subscribe">
	 <div class="container">
		 <h3>Newsletter</h3>
		 <form>
			 <input type="text" class="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
			 <input type="submit" value="Subscribe">
		 </form>
	 </div>
</div>
<!---->
@include('shop.layouts.footer')