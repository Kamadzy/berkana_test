@include('shop.layouts.header')
<!-- check out -->
<div class="container">
	<div class="check-sec">	 
		<div class="col-md-3 cart-total">
			{{--<div class="price-details">--}}
				{{--<h3>Price Details</h3>--}}
				{{--<span>Total</span>--}}
				{{--<span class="total1">6200.00</span>--}}
				{{--<span>Discount</span>--}}
				{{--<span class="total1">10%(Festival Offer)</span>--}}
				{{--<span>Delivery Charges</span>--}}
				{{--<span class="total1">150.00</span>--}}
				{{--<div class="clearfix"></div>				 --}}
			{{--</div>	--}}
			<ul class="total_price">
			   <li class="last_price"> <h4>TOTAL</h4></li>	
			   <li class="last_price"><span>${{$total}}</span></li>
			</ul> 
			<div class="clearfix"></div>
			<div class="clearfix"></div>
			<form method="post" action="/purchase">
				{{csrf_field()}}
				<button class="btn btn-success btn-lg" type="submit" class="order">
					Place Order
				</button>
			</form>
			{{--<div class="total-item">--}}
				{{--<h3>OPTIONS</h3>--}}
				{{--<h4>COUPONS</h4>--}}
				{{--<a class="cpns" href="#">Apply Coupons</a>--}}
			{{--</div>--}}
		</div>
		<div class="col-md-9 cart-items">
			<h1>My Shopping Bag ({{count($cart)}})</h1>
			@foreach($cart as $item)
			<div class="cart-header">
				<div class="close1"> </div>
				<div class="cart-sec simpleCart_shelfItem">
						<div class="cart-item cyc">
							<img src="{{$item->item_info->image}}" class="img-responsive" alt=""/>
						</div>
					   <div class="cart-item-info">
						    <h3><a href="single.html">{{$item->item_info->name}}</a>
								{{--<span>Model No: RL-5511S</span>--}}
							</h3>
							<ul class="qty">
								{{--<li><p>Size : 5</p></li>--}}
								<li class="pull-right">
									<h3>${{$item->item_info->price}} per piece</h3>
									<br>
									<h2 class="pull-right">Subtotal:</h2>
									<br>
									<h2 class="pull-right">${{$item->quantity*$item->item_info->price}}</h2>
								</li>
								<li><p>Qty : {{$item->quantity}}</p></li>
							</ul>
							<div class="delivery">

								 {{--<p>Service Charges : Rs.100.00</p>--}}
								 {{--<span>Delivered in 2-3 bussiness days</span>--}}
								 <div class="clearfix"></div>
							</div>								
					   </div>
					   <div class="clearfix"></div>
											
				  </div>
			 </div>
			@endforeach
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
<!-- //check out -->
<!---->
<div class="subscribe">
	 <div class="container">
		 <h3>Newsletter</h3>
		 <form>
			 <input type="text" class="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
			 <input type="submit" value="Subscribe">
		 </form>
	 </div>
</div>
<!---->
<div class="footer">
	 <div class="container">
		 <div class="footer-grids">
			 <div class="col-md-3 about-us">
				 <h3>About Us</h3>
				 <p>Maecenas nec auctor sem. Vivamus porttitor tincidunt elementum nisi a, euismod rhoncus urna. Curabitur scelerisque vulputate arcu eu pulvinar. Fusce vel neque diam</p>
			 </div>
			 <div class="col-md-3 ftr-grid">
					<h3>Information</h3>
					<ul class="nav-bottom">
						<li><a href="#">Track Order</a></li>
						<li><a href="#">New Products</a></li>
						<li><a href="#">Location</a></li>
						<li><a href="#">Our Stores</a></li>
						<li><a href="#">Best Sellers</a></li>	
					</ul>					
			 </div>
			 <div class="col-md-3 ftr-grid">
					<h3>More Info</h3>
					<ul class="nav-bottom">
						<li><a href="login.html">Login</a></li>
						<li><a href="#">FAQ</a></li>
						<li><a href="contact.html">Contact</a></li>
						<li><a href="#">Shipping</a></li>
						<li><a href="#">Membership</a></li>	
					</ul>					
			 </div>
			 <div class="col-md-3 ftr-grid">
					<h3>Categories</h3>
					<ul class="nav-bottom">
						<li><a href="#">Car Lights</a></li>
						<li><a href="#">LED Lights</a></li>
						<li><a href="#">Decorates</a></li>
						<li><a href="#">Wall Lights</a></li>
						<li><a href="#">Protectors</a></li>	
					</ul>					
			 </div>
			 <div class="clearfix"></div>
		 </div>
	 </div>
</div>
<!---->
<div class="copywrite">
	 <div class="container">
		 <div class="copy">
			 <p>© 2015 Lighting. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
		 </div>
		 <div class="social">							
				<a href="#"><i class="facebook"></i></a>
				<a href="#"><i class="twitter"></i></a>
				<a href="#"><i class="dribble"></i></a>	
				<a href="#"><i class="google"></i></a>	
				<a href="#"><i class="youtube"></i></a>	
		 </div>
		 <div class="clearfix"></div>
	 </div>
</div>
<!---->
</body>
</html>