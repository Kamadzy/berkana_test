@extends('layouts.app')
    @section('content')
    <div class="col-md-12">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Purchase ID</th>
                <th>User email</th>
                <th>Item name</th>
                <th>Quantity</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            @foreach($purchases as $purchase)
                <tr>
                    <td>{{$purchase->id}}</td>
                    <td>{{$purchase->user->email}}</td>
                    <td>{{$purchase->item->name}}</td>
                    <td>{{$purchase->quantity}}</td>
                    <td>{{$purchase->status}}</td>
                    <td>
                        <form method="post" action="/admin/purchases/action">
                            {{csrf_field()}}
                            <input type="hidden" name="cart_id" value="{{$purchase->id}}">
                            <select name="action">
                                <option disabled selected>Select action</option>
                                <option value="purchased">Set processed</option>
                                <option value="declined">Decline</option>
                            </select>
                            <button type="submit" class="btn btn-default">Go!</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    @endsection