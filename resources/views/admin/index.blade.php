@extends('layouts.app')

@section('content')
<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading"><a href="/admin/categories">Categories</a></div>

        <div class="panel-body">
            Categories count : {{\App\Category::count()}}
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading"><a href="/admin/categories">Items</a></div>

        <div class="panel-body">
            Items count : {{\App\Item::count()}}
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading"><a href="#">Manufacturers</a></div>

        <div class="panel-body">
            Manufacturers count : 100500тыщ
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading"><a href="#">Purchases</a></div>

        <div class="panel-body">
            Pending purchases count : {{count($pending_purchases)}}
        </div>
    </div>
</div>
@endsection