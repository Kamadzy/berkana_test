@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$category->name}}</div>

                    <div class="panel-body">
                        <div>
                            <div class="col-md-4">
                                <img src="{{$category->image}}"></img>
                            </div>
                            <div class="col-md-8">
                                <p>{{$category->description}}</p>
                            </div>
                            <br>
                            <br>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">Subcategories</div>
                            <div class="panel-body">
                                @foreach($subcategories as $subcategory)
                                <div class="panel">
                                    <div class="panel-heading"><a href="/admin/categories/show/{{$subcategory->id}}">{{$subcategory->name}}</a>
                                    @include('admin.categories.subcategory_edit')
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-md-4">
                                            <img src="{{$subcategory->image}}">
                                        </div>
                                        <div class="col-md-8">
                                            <p>{{$subcategory->description}}</p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">Items</div>
                            <div class="panel-body">
                                @foreach($items as $item)
                                    <div class="panel">
                                        <div class="panel-heading">{{$item->name}}</div>
                                        <div class="panel-body">
                                            <div class="col-md-4">
                                                <img src="{{$item->image}}">
                                            </div>
                                            <div class="col-md-8">
                                                <p>{{$item->description}}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection