@extends('layouts.app')

@section('content')
    @include('admin.manufacturer.create_modal')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#create_manufacturer">+ New</button>
                    <h1>Manufacturers
                    </h1>
                </div>
                @foreach($manufacturer as $manufacturer)
                    <div class="panel panel-default">
                        <div class="panel-heading">{{$manufacturer->name}}</div>

                        <div class="panel-body">
                            <div class="col-md-2">
                                <img src="/{{$manufacturer->image}}" width="230px">
                            </div>
                            <div class="col-md-8">
                                {{$manufacturer->description}}
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#edit_modal{{$manufacturer->id}}">Edit</button>
                                <form method="post" action="/admin/manufacturers/delete">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{$manufacturer->id}}">
                                    <button type="submit" class="btn btn-danger" style="background-color: #ff0000;">Delete</button>
                                </formmethod="get" action="/admin/manufacturers/delete{{$manufacturer->$admin_destroy}}">
                                <div class="modal fade" id="edit_modal{{$manufacturer->id}}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title">Editing manufacturer #{{$manufacturer->id}}</h4>
                                            </div>
                                            <form method="post" action="/admin/manufacturers/edit" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <input type="hidden" name="id" value="{{$manufacturer->id}}">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label>Name</label>
                                                        <br>
                                                        <input class="form-control" type="text" name="name" value="{{$manufacturer->name}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Image</label>
                                                        <br>
                                                        <input class="form-control" type="text" name="image" value="{{$manufacturer->image}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Parent category</label>
                                                        <br>
                                                        <select class="form-control" name="category_id">
                                                            <option disabled selected>Please select parent category</option>
                                                           
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <br>
                                                        <textarea class="form-control" name="description">{{$manufacturer->description}}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Price</label>
                                                        <br>
                                                        <input class="form-control" type="number" name="price" value="{{$manufacturer->price}}" min="0">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
        </div>
    </div>
@endsection