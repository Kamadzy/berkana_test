@extends('layouts.app')

@section('content')
    @include('admin.items.create_modal')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#create">+ New</button>
                    <h1>Items
                    </h1>
                </div>
                @foreach($items as $item)
                    <div class="panel panel-default">
                        <div class="panel-heading">{{$item->name}}</div>

                        <div class="panel-body">
                            <div class="col-md-2">
                                <img src="/{{$item->image}}" width="230px">
                            </div>
                            <div class="col-md-8">
                                {{$item->description}}
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#edit_modal{{$item->id}}">Edit</button>
                                <form method="post" action="/admin/items/delete">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{$item->id}}">
                                    <button type="submit" class="btn btn-danger" style="background-color: #ff0000;">Delete</button>
                                </form>
                                <div class="modal fade" id="edit_modal{{$item->id}}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title">Editing item #{{$item->id}}</h4>
                                            </div>
                                            <form method="post" action="/admin/items/edit" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <input type="hidden" name="id" value="{{$item->id}}">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label>Name</label>
                                                        <br>
                                                        <input class="form-control" type="text" name="name" value="{{$item->name}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Image</label>
                                                        <br>
                                                        <input class="form-control" type="text" name="image" value="{{$item->image}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Parent category</label>
                                                        <br>
                                                        <select class="form-control" name="category_id">
                                                            <option disabled selected>Please select parent category</option>
                                                            @foreach($categories as $category1)
                                                                <option value="{{$category1->id}}">
                                                                    {{$category1->name}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <br>
                                                        <textarea class="form-control" name="description">{{$item->description}}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Price</label>
                                                        <br>
                                                        <input class="form-control" type="number" name="price" value="{{$item->price}}" min="0">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </form>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
        </div>
    </div>
@endsection