<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Item;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    public function index(){
        $pending_purchases=Cart::where('status','pending')->get()->groupby('user_id');
//        $all_purchases=Cart::all()->groupBy('user_id');

        return view('admin.index',compact('pending_purchases'));
    }

    public function purchases(){
        $purchases=Cart::all();
        foreach ($purchases as $key=>$purchase){
            $item=Item::find($purchase->item_id);
            $user=User::find($purchase->user_id);
            $purchases[$key]->item=$item;
            $purchases[$key]->user=$user;
        }
        return view('admin.purchases.index',compact('purchases'));
    }
    public function purchases_single($id){

        $purchases=Cart::where('cart_id',$id)->get();

        foreach ($purchases as $key=>$purchase){
            $item=Item::find($purchase->item_id);
            $user=User::find($purchase->user_id);
            $purchases[$key]->item=$item;
            $purchases[$key]->user=$user;
        }

        return view('admin.purchases.index',compact('purchases'));
    }

    public function action(Request $request){
        $action=$request->get('action');
        $id=$request->get('cart_id');
        $purchase=Cart::find($id);
        if($action=='purchased'){
            $purchase->status='purchased';
            $purchase->save();
        } else if($action=='declined'){
            $purchase->status='declined';
            $purchase->save();
        }
        return(redirect('/admin/purchases'));
    }

}
