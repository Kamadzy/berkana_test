<?php

namespace App\Http\Controllers;

use App\Category;
use App\Item;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function admin_index(Request $request){
        $items=Item::all();
        $categories=Category::all();
//        return(csrf_token());
        return (view('admin.items.index',compact('items','categories')));
    }

    public function admin_create(Request $request){
        $item=$request->except('_token','image');
        $file=$request->file('image');
//        dd($file);
        if($file){
        $path=storage_path('/images');
        $hash=time();
        $extension = $file->getClientOriginalExtension();
        $file->move($path,$hash.'.'.$extension);
        $item['image']='images/'.$hash.'.'.$extension;
        }
        else{
              $item['image']='no-img.png';
        }

        Model::unguard();
        $new_item=Item::create($item);
        Model::reguard();
//        return ($new_item);
        return redirect('/admin/items');
    }

    public function admin_edit(Request $request){
        $id=$request->get('id');
        $item_edited=$request->except('id','_token','image');
        $file=$request->file('image');
        if($file){
            $path=storage_path('/images');
            $hash=time();
            $extension = $file->getClientOriginalExtension();
            $file->move($path,$hash.'.'.$extension);
            $item['image']=$path.'/'.$hash.'.'.$extension;
        }
//        else{
//            $item['image']='no-img.png';
//        }
        Model::unguard();
        $item=Item::find($id);
        $item->update($item_edited);
        $item->save();
        Model::reguard();
//        return($item);
        return redirect('/admin/items');
    }

    public function admin_destroy(Request $request){
        $id=$request->get('id');
        Model::unguard();
        $item=Item::find($id)->delete();
        Model::reguard();
        return redirect('/admin/items');
    }

}
