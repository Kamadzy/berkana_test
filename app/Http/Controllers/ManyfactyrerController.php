<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manufacturer;
use Illuminate\Database\Eloquent\Model;

class ManyfactyrerController extends Controller
{
     public function admin_index(Request $request)
           {
            // переменная обращается к модели и забирает всё методом ОЛЛ)
            $manufacturer=Manufacturer::all();
            //возвращаем вьюху 
            return (view('admin.manufacturer.index',compact('manufacturer')));
           }

        public function admin_create(Request $request)
        {
        	
        	 // Забираем всё кроме картинки
             $manufacturer=$request->except('image','_token');
             //Забираем файл картинки
             $file=$request->file('image');

            // Проверяем есть ли в запросе файл
        if($file) 
            {
            	// Если файл есть, то 
              $path=storage_path('/images');
              $hash=time();
              $extension = $file->getClientOriginalExtension();
              //Перемещение файла из requst при помощи move
              $file->move($path,$hash.'.'.$extension);
              //Путь у файлу с расширением
              $manufacturer['image']='images/'.$hash.'.'.$extension;

            }

            else()
              {

              }
            // Открываем поля модели для заполнения
           
        Model::unguard();
        // Ответом на метод  create будет возврат да или нет (Создалось значение в базе $new_manufacturer )
        $new_manufacturer=Manufacturer::create($manufacturer);
        // Запрет изменения полей модели
        Model::reguard();
        return redirect('/manufacturer');
        }

        public function admin_edit(Request $request)
          {

            $id=$request->get('id');
            $manufacturer_edited=$request->except('id','image');
            $file=$request->file('image');

        if($file)
          {
            $path=storage_path('/images');
            $hash=time();
            $extension = $file->getClientOriginalExtension();
            $file->move($path,$hash.'.'.$extension);
            $manufacturer['image']=$path.'/'.$hash.'.'.$extension;
          }
        else
          {
            $manufacturer['image']='no-img.png';
          }
        Model::unguard();
        $manufacturer=Item::find($id);
        $manufacturer->update($manufacturer_edited);
        $manufacturer->save();
        Model::reguard();
//        return($manufacturer);
        return redirect('/manufacturer');
        }


        public function admin_destroy(Request $request)
         {
           $id=$request->get('id');
           Model::unguard();
           $item=Manufacturer::find($id)->delete();
           Model::reguard();
           return redirect('/manufacturer');
         }

        public function brands()
        {
          $manufacturer=Manufacturer::all();
          return view ('shop.manufacturer',compact('manufacturer'));
        }


}