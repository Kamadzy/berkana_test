<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Category;
use Illuminate\Database\Eloquent\Model;

class CategoryController extends Controller
{
    public function admin_index(Request $request){
        $displayed_categories=Category::where('parent_category_id',null)->get();
        $categories=Category::all();
        return (view('admin.categories.index',compact('displayed_categories','categories')));
    }

    public function admin_create(Request $request){
        $item=$request->except('_token','image');
        $file=$request->file('image');
//        dd($file);
        if($file){
            $path=storage_path('/images');
            $hash=time();
            $extension = $file->getClientOriginalExtension();
            $file->move($path,$hash.'.'.$extension);
            $item['image']='images/'.$hash.'.'.$extension;
        }
        Model::unguard();
        $new_category=Category::create($item);
      //  Model::reguard();
//        return ($new_item);
        return redirect('/admin/categories');
    }

    public function admin_edit(Request $request)
    {
//          dd($request);
          $id=$request->get('id');
          $category_edited=$request->except('id','_token','image');
          $file=$request->file('image');
//          dd($file);
        if($file)
          {
              $path=storage_path('/images');
              $hash=time();
              $extension = $file->getClientOriginalExtension();
              $file->move($path,$hash.'.'.$extension);
              $category_edited['image']='images/'.$hash.'.'.$extension;
          }

        else
            {
              $category['image']='no-img.png';
            }

          $category=Category::find($id);
          Model::unguard();
          $category->update($category_edited);
          $category->save();
          Model::reguard();
        return redirect('/admin/categories');
    }

    public function admin_destroy(Request $request)
        {
          $id=$request->get('id');
          Model::unguard();
          $category=Category::find($id)->delete();
          Model::reguard();
        return redirect('/admin/categories');
        }

    public function admin_show($id)
        {
          $category=Category::find($id);
          $subcategories=$category->subcategories()->get();
          $items=$category->items()->get();
          $categories=Category::all();
        return view('admin.categories.show',compact('category','subcategories','items','categories'));
        }

    public function category_show()
        {
          $category_show=Category::all();
          return view ('shop.category',compact('category_show'));
        }

    
}
