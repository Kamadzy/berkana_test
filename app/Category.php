<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function parent_category(){
        return $this->belongsTo('\App\Category','parent_category_id');
    }

    public function subcategories(){
        return $this->hasMany('\App\Category','parent_category_id');
    }

    public function items(){
        return $this->hasMany('\App\Item','category_id');
    }

   
}
